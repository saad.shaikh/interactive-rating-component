import React from 'react'

const footer = () => {
    return (
        <footer className="attribution">
            Challenge by <a href="https://saad-shaikh-interactive-rating-component.netlify.app/" target="_blank" rel="noreferrer">Frontend Mentor</a>.
            Coded by <a href="https://saad-shaikh-portfolio.netlify.app/" rel="noreferrer">Saad Shaikh</a>.
        </footer>
    )
}

export default footer