import React, { Fragment } from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Footer from '../components/footer'
import rating from '../public/logic'

const Home = () => {
  const selectRating = (e: any) => {
    rating.selected = e.target.innerHTML
    for (let i = 1; i <= 5; i++) {
      if (e.target.innerHTML == i)
        document.getElementById(i.toString())?.classList.add("selected-rating")
      else document.getElementById(i.toString())?.classList.remove("selected-rating")
    }
    document.getElementById("submit")?.classList.add("submit-activate")
  }
  const submitRating = () => {
    rating.submitted = rating.selected
  }

  return (
    <Fragment>

      <Head>
        <title>Interactive rating component - Home page</title>
      </Head>

      <main>
        <div className='card'>
          <div className='star'></div>
          <h2>How did we do?</h2>
          <p>
            Please let us know how we did with your support request. All feedback is appreciated
            to help us improve our offering!
          </p>
          <section className='ratings'>
            <button id='1' className='ratings-button' onClick={selectRating}>1</button>
            <button id='2' className='ratings-button' onClick={selectRating}>2</button>
            <button id='3' className='ratings-button' onClick={selectRating}>3</button>
            <button id='4' className='ratings-button' onClick={selectRating}>4</button>
            <button id='5' className='ratings-button' onClick={selectRating}>5</button>
          </section>
          <Link href="/thank-you">
            <input id='submit' className='submit' type="submit" value="SUBMIT" onClick={submitRating} />
          </Link>
        </div>
      </main>

      <Footer />

    </Fragment>
  )
}

export default Home
