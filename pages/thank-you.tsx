import React, { Fragment } from 'react'
import Head from 'next/head'
import Footer from '../components/footer'
import Logic from "../public/logic"

const thankyou = () => {
    console.log(Logic.submitted)

    return (
        <Fragment>
            <Head>
                <title>Interactive rating component - Thank you page</title>
            </Head>

            <main>
                <div className='thankyou'>
                    <div className='thankyou-image'></div>
                    <h2>You selected {Logic.submitted} out of 5</h2>
                    <h3>Thank you!</h3>
                    <p>
                        We appreciate you taking the time to give a rating. If you ever need more support,
                        don’t hesitate to get in touch!
                    </p>
                </div>
            </main>

            <Footer />

        </Fragment>
    )
}

export default thankyou