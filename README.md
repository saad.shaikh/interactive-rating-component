# Title: Interactive rating component

Tech stack: Next.js & TypeScript

Deployed project: https://saad-shaikh-interactive-rating-component.netlify.app/

## Main tasks:
- Created a rating feature where the user can submit a rating out of 5, which will take you to a "Thank you" page
- Made the submit button disabled by default so that it is only enabled once a rating has been selected


## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
